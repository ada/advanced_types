#!/usr/bin/env python
from setuptools import setup, find_packages
from setup_parse_requirements import parse_requirements

version = __import__("advanced_types").VERSION

setup(
    name="advanced-types",
    version=version,
    url="http://asta-s.eu",

    download_url="TODO",
    dependency_links=[
        "git+https://bitbucket.org/ada/advanced_types.git#egg=advanced_types"
    ],
    author_email="ada@asta-s.eu",
    author="Andrei Danciuc",
    description="Advanced python dict",
    license="TODO",
    #long_description=open("README.rst").read(),
    packages=[
        "advanced_types"
    ],
    package_data={
        "": ["*.py", "requirements.txt"]
    },
    include_package_data=True,
    install_requires=parse_requirements("requirements.txt"),
    classifiers=[
        "Intended Audience :: Developers",
        "Topic :: Software Development :: Libraries :: Python Modules",
        "Programming Language :: Python",
        "Programming Language :: Python :: 2.7",
        "Programming Language :: Python :: 3",
    ],
)
