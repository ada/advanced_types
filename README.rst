Advanced Types
-------------

Features:
  - AdvancedDict
      - load/save json from/to string, file, db ...
      - querying dict::
         instead of vocabulary["a"]["b"]["c"]["d"] (it is support also)
         do vocabulary["a.b.c.d"]
      - dynamic creating::
         >>> vocabulary = AdvancedDict()
         >>> vocabulary["a.b.c.d"] = 10
         >>> print vocabulary
         {"a": {"b": {"c": {"d": 10}}}}
      - pretty print
         >>> print vocabulary.get_pretty()
         {
             "a":
             {
                  "b" : 10
             }
         }
  - order dict wrapper AdvancedDictOrdered()
  - AdvancedSequentialSet use compact storage of unique set numbers
    instead of [1, 2, 3, 4, 5, 10, 11, 12, 13, 87]
    save it as [(1, 5), (10, 13), 87]

Install:

    >>> sudo /opt/python-virtualenv/alfa/bin/pip install -e git+https://ada@bitbucket.org/ada/advanced_dict.git#egg=dvanced_dict
