#!/usr/bin/env python
from __future__ import unicode_literals
from __future__ import print_function

import json

from advanced_types.advanced_dict import AdvancedDict
from collections import OrderedDict
from advanced_types.common import JsonException


class AdvancedOrderedDictException(JsonException):
    pass


class AdvancedOrderedDict(OrderedDict, AdvancedDict):
    """
    wrapper for dictionary data type

    get_pretty ################
    > >> a = AdvancedOrderedDict('{"++" : {"b": "++"}}')
    > >> print(a.get_pretty())
    {
        "++": {
            "b": "++"
        }
    }
    > >> print(a.get_pretty("++.b"))
    "++"
    > >> a = AdvancedOrderedDict(a)
    > >> print(a)
    {"++": {"b": "++"}}
    """

    def __init__(self, *args, **kw):
        """
        >>> print(AdvancedOrderedDict({"--" : "--"}))
        {"--": "--"}

        >>> a = AdvancedOrderedDict()
        >>> print(a)
        {}

        >>> a = AdvancedOrderedDict(None)
        >>> print(a)
        {}
        """
        if args == (None, ) and kw == {}:  # AdvancedDict(None)
            args = ()

        if len(args) and isinstance(args[0], str):
            args = (json.loads(args[0]),)

        super(AdvancedOrderedDict, self).__init__(*args, **kw)

    def __setitem__(self, key, value, *args):
        """
        >>> a = AdvancedOrderedDict()
        >>> a["a.b1"] = ""
        >>> a["a.b3"] = ""
        >>> a["a.b2"] = ""
        >>> print(a.get_pretty())  #doctest: +NORMALIZE_WHITESPACE
        {
            "a": {
                "b1": "",
                "b3": "",
                "b2": ""
            }
        }
        """
        AdvancedDict.__setitem__(self, key, value, _inherit_from=OrderedDict, *args)

    def force_get(self, *arg):
        """
        force to get item as AdvancedOrderedDict
        """
        res = self.get(*arg)
        if not isinstance(res, dict):
            return res

        return AdvancedOrderedDict(res)

    def __delitem__(self, key, *args):
        """
        >>> a = AdvancedOrderedDict({"b" : 1})
        >>> a["lol.samples"] = ["Inspiron"]
        >>> a["lol.brb"] = 666
        >>> print(a)
        {"b": 1, "lol": {"samples": ["Inspiron"], "brb": 666}}
        >>> del a["lol.samples"]
        >>> print(a)
        {"b": 1, "lol": {"brb": 666}}
        """
        AdvancedDict.__delitem__(self, key, _inherit_from=OrderedDict, *args)

    def load_json(self, file_path, clear=True, raise_file_not_fount=False):
        """
        >>> a = AdvancedOrderedDict({"b" : "a"})
        >>> a.update({
        ... "accounts": [
        ...     {  "status" : "Use a phone to verify your account" },
        ...     {  "pass"   : "N0ise-Full"                         }
        ... ]
        ... })
        >>> path = "/tmp/ciur_test_advanced_ordered_dict.json"
        >>> a.dump_json(path)
        >>> dev_null = a.load_json(path)
        >>> print(dev_null.get_pretty()) #doctest: +NORMALIZE_WHITESPACE
        {
            "b": "a",
            "accounts": [
                {
                    "status": "Use a phone to verify your account"
                },
                {
                    "pass": "N0ise-Full"
                }
            ]
        }
        """
        return AdvancedDict.load_json(self, file_path, clear, raise_file_not_fount, OrderedDict)

    def __str__(self):
        """
        >>> a = AdvancedOrderedDict()
        >>> a["a.b1"] = ""
        >>> a["a.b3"] = ""
        >>> a["a.b2"] = ""
        >>> print(a)
        {"a": {"b1": "", "b3": "", "b2": ""}}
        """
        return AdvancedDict.__str__(self)

    def get_pretty(self, key_path=None, indent=4):
        return AdvancedDict.get_pretty_(cls=self, key_path=key_path, indent=indent, sort_keys=False)
