from __future__ import absolute_import

import re
import json
import decimal
import datetime
import sys


try:
    from pymongo.objectid import ObjectId
except (ImportError, ) as e:
    try:
        # noinspection PyUnresolvedReferences
        from bson.objectid import ObjectId
    except (ImportError, ) as e:
        pass


class JsonException(Exception):
    """
    Exception that raise with dictionary explication
    >> raise JsonException({"key_name1": "val1"})
    Traceback (most recent call last):
        ...
    common.JsonException: {'key_name1': 'val1'}
    """
    def __init__(self, *args, **kwargs):
        super(JsonException, self).__init__( *args, **kwargs)
        if args:
            self.value = args[0]


    def __str__(self):
        return repr(self.value)


def dt_handler(obj):
    """
    convert all data fields in json intro string format
    """
    if isinstance(obj, re.compile("").__class__):
        return "SRE_Pattern(%s)" % obj.pattern

    if isinstance(obj, decimal.Decimal):
        return "Decimal(%s)" % obj

    if sys.version < '3' and isinstance(obj, unicode):
        return obj.encode("utf-8")

    if isinstance(obj, datetime.datetime):
        return "Date(%s)" % obj.isoformat()

    if isinstance(obj, ObjectId):
        return "ObjectId('%s')" % str(obj)

    if isinstance(obj, type):
        return str(obj)

    return type(obj)


def json_dump(obj, sort_keys = False, indent = 4):
    """
    pretty formatting json
    """
    return json.dumps(
        obj,
        sort_keys    = sort_keys,
        indent       = indent,
        default      = dt_handler,
        ensure_ascii = False
    )