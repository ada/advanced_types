#!/usr/bin/env python
from __future__ import unicode_literals
from __future__ import print_function

import os
import re
import json
import copy
import decimal

from advanced_types.common import json_dump
from advanced_types.common import JsonException
from advanced_types.python_compatible_2_and_3 import *

class AdvancedDictException(JsonException):
    pass


class AdvancedDict(dict):
    """
    wrapper for dictionary data type
    """

    _SRE_PATTERN_CLASS = re.compile("").__class__
    _JSON_ERROR_PATTERN = re.compile(u"(?s),\s*[\]\}]")

    def __init__(self, *args, **kw):
        """
        >>> print(AdvancedDict({"--" : "--"}).get_pretty())
        {
            "--": "--"
        }
        >>> AdvancedDict().get_pretty()
        '{}'

        >>> AdvancedDict(None).get_pretty()
        '{}'
        """

        if args == (None, ) and kw == {}: # AdvancedDict(None)
            args = ()

        if len(args) and isinstance(args[0], basestring):
            args = (json.loads(args[0]),)

        super(AdvancedDict, self).__init__(*args, **kw)

    def __add__(self, x):
        """
        {} + {}
        """
        a = dict(self, **x)
        return AdvancedDict(a)

    def __sub__(self, x):
        """
        {} - {}
        """
        if isinstance(x, basestring):
            dict.__delitem__(self, x)

        return self

    def __getitem__(self, key):
        """
        {"a" : {"b" : "c"}}["a.b"] == c
        """
        if not isinstance(key, basestring):
            return dict.__getitem__(self, key)

        # root path  .root.children
        if key.startswith("."):
            key = key[1:]

        keys = key.split(".")

        tmp = dict.__getitem__(self, keys[0])
        for i_key in keys[1:]:
            tmp = tmp.__getitem__(i_key)

        return tmp

    def has_key(self, key):
        """
        {"a" : {"b" : "c"}}.has_keys("a.b") == True
        """
        if not isinstance(key, basestring):
            return dict.__contains__(self, key)

        # root path  .root.children
        if key.startswith("."):
            key = key[1:]

        keys = key.split(".")

        if not dict.__contains__(self, keys[0]):
            return False

        tmp = dict.__getitem__(self, keys[0])
        for i_key in keys[1:]:
            if not tmp.has_key(i_key):
                return False
            tmp = tmp.__getitem__(i_key)

        return True

    def __contains__(self, key):
        return self.has_key(key)

    def get(self, key, default=None):
        """
        {"a" : {"b" : "c"}}.get("a.c", "0") == "0"
        """

        # root path  .root.children
        if key.startswith("."):
            key = key[1:]

        keys = key.split(".")

        if keys[0] not in self:
            return default

        tmp = dict.__getitem__(self, keys[0])

        for i_key in keys[1:]:
            if not (isinstance(tmp, dict) and tmp.has_key(i_key)):
                return default
            tmp = tmp.__getitem__(i_key)

        return tmp

    def get_utf8(self, key, default=None):
        """
        ensure to receive "utf-8" string not unicode
        {"a" : {"b" : "c"}}.get("a.c", "0") == "0"

        """
        tmp = self.get(key, default)

        tmp = tmp.encode("utf-8") if isinstance(tmp, unicode) else tmp
        return tmp

    def __setitem__(self, key, value, _inherit_from=dict, *args):
        """
        >>> a = AdvancedDict({"a": {"b1": "", "b3": ""}})
        >>> a["a.b2"] = ""
        >>> print(a.get_pretty()) #doctest: +NORMALIZE_WHITESPACE
        {
            "a": {
                "b1": "",
                "b2": "",
                "b3": ""
            }
        }

        # TODO
        >> a = AdvancedDict()
        >> a['date'] = ""
        >> a['items.usd.buy'] = ""
        >> a['items.usd.sell'] = ""
        >> a['items.try.buy'] = ""
        >> a['items.try.sell'] = ""
        >> a['items.chf.buy'] = ""
        >> a['items.chf.sell']= ""
        """
        i = 10
        def _create_new(keys_):
            tmp0 = _inherit_from()
            tmp0[keys_[-1]] = value

            for i_key_ in keys_[-2:0:-1]:
                tmp_ = _inherit_from()
                tmp_[i_key_] = tmp0
                tmp0 = tmp_

            return tmp0

        if not isinstance(key, basestring):
            _inherit_from.__setitem__(self, key, value, *args)
            return

        # root path  .root.children
        if key.startswith("."):
            key = key[1:]

        keys = key.split(".")

        if keys.__len__() == 1:
            _inherit_from.__setitem__(self, keys[0], value, *args)
        else:
            tmp = _inherit_from.get(self, keys[0])
            if not tmp:
                _inherit_from.__setitem__(self, keys[0], _create_new(keys), *args)
            else:
                for i_key in keys[1:-1]:
                    tmp = tmp.__getitem__(i_key)

                tmp.__setitem__(keys[-1], value)

    def __delitem__(self, key, args=None, _inherit_from=dict):
        """
        del {"a" : {"b" : "c", "c" : 10}}["a.c"]
        {"a" : {"b" : "c"}
        """
        # root path  .root.children
        if not args:
            args = tuple()

        if key.startswith("."):
            key = key[1:]

        keys = key.split(".")

        if keys.__len__() == 1:
            _inherit_from.__delitem__(self, keys[0], *args)
        else:
            tmp = _inherit_from.__getitem__(self, keys[0])
            for i_key in keys[1:-1]:
                tmp = tmp.__getitem__(i_key)

            tmp.__delitem__(keys[-1])

    @classmethod
    def _recursive_key_to_string_repr(cls, obj):
        """
        enhance json.dump because
        json.dump can accept only default key type as:
        - str, unicode,
        - int, long, float,
        - bool
        - NoneType
        """
        if isinstance(obj, (basestring, int, long, float, bool, type(None), cls._SRE_PATTERN_CLASS)):
            return obj

        obj_ = copy.copy(obj)

        if isinstance(obj_, dict):
            map_ = obj_.__class__()
            for k_old in obj_.keys():

                if isinstance(k_old, cls._SRE_PATTERN_CLASS):
                    k_new = "_%s('%s')_" % (cls._SRE_PATTERN_CLASS.__name__, k_old.pattern)
                elif isinstance(k_old, (list, basestring, int, long, float, bool, type(None))):
                    k_new = k_old
                else:
                    k_new = "Unhandled_type('%s')" % repr(k_old)

                map_[k_new] = cls._recursive_key_to_string_repr(obj_[k_old])

            return map_

        if isinstance(obj_, (list, tuple)):
            for index, value in enumerate(obj_):
                obj_[index] = cls._recursive_key_to_string_repr(value)

            return obj_

        raise NotImplemented

    @staticmethod
    def get_pretty_(cls, key_path=None, indent=4, sort_keys=True):
        # root path  .root.children
        if key_path and key_path.startswith("."):
            key_path = key_path[1:]

        dict_ = cls[key_path] if key_path else cls

        try:
            str_ = json_dump(dict_, indent=indent, sort_keys=sort_keys)
        except (TypeError, ):
            dict_ = cls._recursive_key_to_string_repr(dict_)
            str_ = json_dump(dict_, indent=indent)

        return str_

    def get_pretty(self, key_path=None, indent=4):
        """
        json_dump(self).encode("utf-8")

        http://docs.python.org/library/doctest.html#doctest.NORMALIZE_WHITESPACE
        for doctest use: #doctest: +NORMALIZE_WHITESPACE

        >>> a = AdvancedDict('{"++" : {"b": "++"}}')
        >>> print(a.get_pretty())
        {
            "++": {
                "b": "++"
            }
        }
        >>> print(a.get_pretty("++.b"))
        "++"
        >>> print(AdvancedDict(a).get_pretty())
        {
            "++": {
                "b": "++"
            }
        }
        """
        return AdvancedDict.get_pretty_(cls=self, key_path=key_path, indent=indent)

    def rename_key(self, old_key, new_key, cast = None, mandatory=True):
        """
        self.rename("old", "new")
        self.rename("a.old", "new1")
        self.rename("old", "a.new2")
        """

        if mandatory or self.has_key(old_key):
            if cast:
                self[new_key] = cast(self[old_key])
            else:
                self.__setitem__(new_key, self.__getitem__(old_key))

            self.__delitem__(old_key)

        else: #don't raise error if key don't exist
            #print "skip rename"
            pass

    def rename_keys(self, arg_keys):
        """
        self.rename({ "from1" : ["into1", cast], "y.from2" : "g.into2"})
        or
        self.rename(( "from1" , "into1", cast, "y.from2" , "g.into2"))
        or
        self.rename([ "from1" , "into1", cast, "y.from2" , "g.into2"])
        """
        if isinstance(arg_keys, tuple) or isinstance(arg_keys, list):
            arg_keys_len = len(arg_keys)
            i = 0
            while i < arg_keys_len:
                if isinstance(arg_keys[i], type):
                    #print "!!", map_keys[i+1], map_keys[i+2], map_keys[i]
                    self.rename_key(arg_keys[i+1], arg_keys[i+2], arg_keys[i])
                    i += 3
                else:
                    #print "!", map_keys[i], map_keys[i+1]
                    self.rename_key(arg_keys[i], arg_keys[i+1])
                    i += 2
        elif isinstance(arg_keys, dict) or isinstance(arg_keys, AdvancedDict):
            for key, val in iteritems23(arg_keys):
                if isinstance(val, basestring):
                    self.rename_key(key, val)
                else:
                    self.rename_key(key, val[0], cast=val[1])

    def _load_json_from_str(self, literal_str, object_pairs_hook=None):
        try:
            return json.loads(
                literal_str,
                object_pairs_hook=object_pairs_hook,
                parse_float=decimal.Decimal
            )

        except (ValueError, ) as e:
            m = re.search(self._JSON_ERROR_PATTERN, literal_str)
            if m:
                raise AdvancedDictException({
                    "msg": "No JSON object could be decoded",
                    "suggestion": "forgot to delete comma separator ;)",
                    "file_path": literal_str
                })

            raise AdvancedDictException({
                "msg": "No JSON object could be decoded",
                "file_path": literal_str
            })

    def load_json(self, file_path, clear=True, raise_file_not_fount=False, object_pairs_hook=None):
        """
        loaf data from file
        @param
            @clear - clear prev data
            @raise_file_not_fount - raise error if file cant be found
        >>> a = AdvancedDict({
        ...    "accounts": [
        ...        {
        ...        "email": "vector_soft_developer_test@yahoo.com",
        ...        "id": 0,
        ...        "pass": "Trust9",
        ...        "status": "Use a phone to verify your account"
        ...        },
        ...        {
        ...        "email": "vectorsoftX@hotmail.com",
        ...        "id": 1,
        ...        "pass": "N0ise-Full",
        ...        "status": "active"
        ...        }
        ...    ]
        ... })
        >>> path = "/tmp/ciur_test_advanced_dict.json"
        >>> a.dump_json(path)
        >>> print(a.get_pretty())  # doctest: +NORMALIZE_WHITESPACE
        {
            "accounts": [
                {
                    "email": "vector_soft_developer_test@yahoo.com",
                    "id": 0,
                    "pass": "Trust9",
                    "status": "Use a phone to verify your account"
                },
                {
                    "email": "vectorsoftX@hotmail.com",
                    "id": 1,
                    "pass": "N0ise-Full",
                    "status": "active"
                }
            ]
        }
        """
        dict_obj = {}
        if os.path.isfile(file_path):
            with open(file_path, "r") as f:
                db_config = f.read()

            dict_obj = self._load_json_from_str(db_config, object_pairs_hook)
        else:
            if raise_file_not_fount:
                raise AdvancedDictException({
                    "msg": "json file cant be found",
                    "file_path": file_path
                })

        if clear:
            self.clear()

        self.__init__(dict_obj)

        return self

    def load_json_from_str(self, literal_str, clear=True, object_pairs_hook=None):
        dict_obj = self._load_json_from_str(literal_str, object_pairs_hook)

        if clear:
            self.clear()

        self.__init__(dict_obj)

        return self

    def dump_json(self, file_path, sort_keys = False):
        """
        save data to file
        """
        str_js = json_dump(self, sort_keys = sort_keys)
        if isinstance(str_js, basestring):
            str_js = str_js.encode("utf-8")
        open(file_path, "wb").write(str_js)

    @classmethod
    def pretty_float(cls, self_test):
        """
        convert {"k" : Decimal(0.5)} into {"k" : 0.5 }
        """
        if isinstance(self_test, dict):
            for k, v in iteritems23(self_test):
                if isinstance(v, dict):
                    self_test[k] = cls.pretty_float(v)
                elif isinstance(v, list):
                    self_test[k] = [cls.pretty_float(i) for i in v]
                elif isinstance(v, decimal.Decimal):
                    self_test[k] = float(v)
                elif isinstance(v, (basestring, int, bool, float, long)):
                    self_test[k] = v
                else:
                    raise NotImplemented
        else:
            if isinstance(self_test, decimal.Decimal):
                return float(self_test)
            if isinstance(self_test, (basestring, int, bool, float, long)):
                return self_test
            else:
                raise NotImplemented

        return self_test

    def __str__(self):
        return self.get_pretty(indent=None)

    def comparable(self):
        return AdvancedDict(self.__str__())